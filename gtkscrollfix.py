#!/usr/bin/python
"""
GTK+ 3 SCROLLBAR "ZOOM MODE" DELAY MODIFICATION TOOL
----------------------------------------------------

This program provides a simple GUI for amending the GTK+ 3 zoom mode scrollbar
behaviour described here:

<https://blogs.gnome.org/mclasen/2013/08/05/scrolling-in-gtk/>

Although this feature cannot be disabled, the "activation delay" can be changed
to a higher value (such as 30 seconds) so that it's unlikely to be triggered by
accident, if at all. Thanks to Hashem Nasarat for providing details about this:

<https://bugzilla.gnome.org/show_bug.cgi?id=728739#c9>

This program should work identically for Python 2 and Python 3.
"""

try:
    # Python 3.x
    from tkinter import *
    from tkinter import messagebox
    import configparser
except ImportError:
    # Python 2.x
    from Tkinter import *
    import tkMessageBox as messagebox
    import ConfigParser as configparser
import os


class TkGUI(object):
    """The main GUI class."""

    def __init__(self):
        """Load the current config file and set up the GUI."""
        self.path = os.path.join(os.path.expanduser("~"), ".config", "gtk-3.0")
        self.filename = os.path.join(self.path, "settings.ini")
        self.config = configparser.RawConfigParser()
        self.config.read(self.filename)

        self.tk = Tk()
        self.tk.title("Scrollfix for GTK3")

        Label(self.tk, text="Scrollbar 'zoom' delay (seconds):").pack()

        try:
            init_value = self.config.getint("Settings", "gtk-long-press-time")
        except configparser.Error:
            init_value = 5000  # Assume 5 seconds by default.

        self.seconds = Scale(self.tk, from_=1, to=30, orient=HORIZONTAL)
        self.seconds.set(init_value // 1000)
        self.seconds.pack(fill=X)

        Button(self.tk, text="Save changes", command=self.on_save).pack()

        self.tk.update()
        self.tk.resizable(width=FALSE, height=FALSE)

    def on_save(self):
        """Button click method - write the config changes to file."""
        press_time = int(self.seconds.get() * 1000)
        if not self.config.has_section("Settings"):
            self.config.add_section("Settings")
        self.config.set("Settings", "gtk-long-press-time", str(press_time))

        # Create the output directory if it does not already exist.
        if not os.path.isdir(self.path):
            try:
                os.makedirs(self.path)
            except Exception as e:
                messagebox.showerror("Could not create directory", str(e))
                return

        # Save the updated config to file.
        try:
            with open(self.filename, "w") as out:
                self.config.write(out)
        except Exception as e:
            messagebox.showerror("Could not save file", str(e))
            return

        messagebox.showinfo("Saved", "Your changes have been saved.")

    def mainloop(self):
        """Enter the main program loop."""
        self.tk.mainloop()

if __name__ == "__main__":
    TkGUI().mainloop()
